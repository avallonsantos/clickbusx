const DATES = [
    {
        "date": "2020-08-21",
        "formattedDate": "21/08/2020"
    },
    {
        "date": "2020-08-23",
        "formattedDate": "23/08/2020"
    },
    {
        "date": "2020-08-24",
        "formattedDate": "24/08/2020"
    },
    {
        "date": "2020-08-27",
        "formattedDate": "27/08/2020"
    },
    {
        "date": "2020-08-28",
        "formattedDate": "28/08/2020"
    },
    {
        "date": "2020-08-30",
        "formattedDate": "30/08/2020"
    },
    {
        "date": "2020-08-31",
        "formattedDate": "31/08/2020"
    },
    {
        "date": "2020-09-03",
        "formattedDate": "03/09/2020"
    },
    {
        "date": "2020-09-04",
        "formattedDate": "04/09/2020"
    },
    {
        "date": "2020-09-06",
        "formattedDate": "06/09/2020"
    },
    {
        "date": "2020-09-07",
        "formattedDate": "07/09/2020"
    },
    {
        "date": "2020-09-10",
        "formattedDate": "10/09/2020"
    },
    {
        "date": "2020-09-11",
        "formattedDate": "11/09/2020"
    },
    {
        "date": "2020-09-13",
        "formattedDate": "13/09/2020"
    },
    {
        "date": "2020-09-14",
        "formattedDate": "14/09/2020"
    },
    {
        "date": "2020-09-17",
        "formattedDate": "17/09/2020"
    },
    {
        "date": "2020-09-18",
        "formattedDate": "18/09/2020"
    }
];

/**
 * Display dates
 */
export const buildDates = (triggerDates) => {
    const options = DATES.map((date) => `<option value="${date.date}">${date.formattedDate}</option>`);
    triggerDates.forEach((triggerDate => {
        triggerDate.insertAdjacentHTML('beforeend', options);
    }));

    $('select').material_select();
};

export const updateClickBusURLBasedOnDate = (date, target) => {
    const link = target.attr('href').split('=')[0];
    const newURL = `${link}=${date}`;
    target.attr('href', newURL);
};
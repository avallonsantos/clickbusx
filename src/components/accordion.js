const accordions = document.querySelectorAll('.accordion-title');

const CreateAccordions = () => {
    accordions.forEach((accordion, index) => {
        accordion.addEventListener('click', () => {
            accordion.classList.toggle('active');

            // Get last div
            let panel = accordion.nextElementSibling;

            if(panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + 'px';
            }
        })
    });
};

export default CreateAccordions;
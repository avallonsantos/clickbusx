const SALES_FORCE_CONFIG = {
    url: 'http://cl.s6.exct.net/DEManager.aspx',
    clientId: '6276011',
    externalKey: '18AF6E6C-FD0A-4629-9AC8-2CC214840171',
    action: 'add',
};


/**
 * Send new lead to database
 * @param email
 * @returns {Promise<void>}
 */
export const sendNewsletter = async (email) => {
    const { url, action, clientId, externalKey } = SALES_FORCE_CONFIG;
    try {
        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                _clientID: clientId,
                _deExternalKey: externalKey,
                _action: action,
                _returnXML: 0,
                _successURL: 'javascript:void(0)',
                _errorURL: 'javascript:void(0)',
                'e-mail': email
            })
        });
        return response;
    } catch (e) {
        alert('Houve um erro ao enviar a mensagem, por favor tente novamente.');
        console.log(e)
    }
}
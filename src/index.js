import 'regenerator-runtime/runtime';

import './assets/scss/app.scss';

import { buildDates, updateClickBusURLBasedOnDate } from './components/utils';
import CreateAccordions from "./components/accordion";
import { sendNewsletter } from "./components/newsletter";


const landingPageContent = document.getElementById('landing-page-content');

const runLandingPageApplication = () => {
    document.addEventListener('DOMContentLoaded', () => {
        const salesForceSuccessUrlInput = document.querySelector('[name="_successURL"]');
        const salesForceErrorUrlInput = document.querySelector('[name="_errorURL"]');
        const availableDates = document.querySelectorAll('.available-dates');

        const location = window.location.href;
        const firstPartURL = location.split('?')[0];

        const params = new URLSearchParams(window.location.search);
        const hasSuccessInParams = params.has('success');

        if(hasSuccessInParams) {
            const successAsString = params.get('success').toString();
            params.delete('success');
            window.location.href = firstPartURL;
            return successAsString === 'true' ?
                alert('Cadastrado com sucesso. A ClickBus agradece') :
                alert('Houve um erro ao cadastrar. Por favor tente novamente.')
        }

        // Updating callback urls
        salesForceSuccessUrlInput.value = `${firstPartURL}?success=true`;
        salesForceErrorUrlInput.value = `${firstPartURL}?success=false`;

        // Display dates
        buildDates(availableDates);

        // Updating date on select change
        $('#table-offers select').on('change', function() {
            const option = $(this).val();
            const row = $(this).data('row');
            const tableRow = $('#table-offers tr')[row];
            const tableRowLink = $(tableRow).find('a');
            updateClickBusURLBasedOnDate(option, tableRowLink)
        });


        // Accordions
        CreateAccordions();

    });
}

if(!landingPageContent) {
    throw new Error('The page\'s main wrapper could not be found. Please wrap the entire application within an element with id "#landing-page-content". So we guarantee that we will not have any kind of conflict.')
}

runLandingPageApplication();
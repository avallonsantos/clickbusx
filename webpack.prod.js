// Importing required dependencies

const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlTemplateConfig = require('./config/template');

// Defining S3 landing page URL

const PROD_S3_LANDING_PAGE_URL = 'https://static.clickbus.com/live/ClickBus/landing-pages/2020/clickbusx/V2/desktop/';

// Config to display CMS placeholders

HtmlTemplateConfig.renderPlaceholders = true;

// Webpack Production configuration

module.exports = merge(common, {
    mode: 'production',
    output: {
        publicPath: PROD_S3_LANDING_PAGE_URL
    },
    plugins: [
        new HtmlWebpackPlugin(HtmlTemplateConfig)
    ]
});

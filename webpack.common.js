// Importing required dependencies

const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Metas = require('./config/metas');
const HtmlTemplateConfig = require('./config/template');

// Webpack configuration

module.exports = {
    node: {
        fs: "empty"
    },
    entry: path.resolve(__dirname, './src/index.js'),
    output: {
        filename: "clickbus.lp.bundle.js",
        path: path.resolve(__dirname, './dist'),
        publicPath: ''
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env'
                    ],
                    plugins: ['@babel/plugin-proposal-class-properties']
                }
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(c|d|t)sv$/,
                use: ['dsv-loader']
            },
            {
                test: /\.(png|jpge|jpg|svg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'img/'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'clickbus.lp.style.css'
        }),
        // new webpack.ProvidePlugin({
        //         //     $: "jquery",
        //         //     jQuery: "jquery"
        //         // })
    ]
};
// Importing required dependencies

const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlTemplateConfig = require('./config/template');

// Defining server port

const APP_PORT = 9001;

// Inserting ClickBus CSS

const scripts = [
    'https://www.clickbus.com.br/static/js/vendors.min.js?v=1.0-622',
    'https://www.clickbus.com.br/static/js/desktop_main.min.js?v=1.0-622',
    'https://www.clickbus.com.br/static/js/components.min.js?v=1.0-622',
    'https://www.clickbus.com.br/static/js/datalayer.min.js?v=1.0-622'
]

scripts.forEach(script => {
    HtmlTemplateConfig.scripts.push({
        src: script
    });
});

HtmlTemplateConfig.links.push({
    rel: 'stylesheet',
    href: 'https://www.clickbus.com.br/static/css/brazil_main.min.css?v=1.0-622'
});

// Webpack Dev configuration

module.exports = merge(common, {
    mode: 'development',
    optimization: {
        minimize: false
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        index: 'index.html',
        compress: false,
        port: APP_PORT
    },
    plugins: [
        new HtmlWebpackPlugin(HtmlTemplateConfig)
    ]
});